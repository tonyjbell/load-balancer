# Instructions:

**Youtube Tutorial Video:**\
https://youtu.be/oEzpXGo35sw

Spin up a load-balancer linux host.

Generate SSH keypair on load-balancer:\
`ssh-keygen`

Spin up 3 app web servers, and place load-balancer public key in `~/.ssh/authorized_keys` on each one.

Modify local DNS on load-balancer in /etc/hosts:
```
x.x.x.x app-1.techwizardoffortune.com
x.x.x.x app-2.techwizardoffortune.com
x.x.x.x app-3.techwizardoffortune.com
```

Update, install git, and clone repo:\
`apt-get update && apt-get install git -y && git clone https://gitlab.com/tutorials47/web/nginx/load-balancer && cd load-balancer`

Install ansible:\
`apt-get install ansible -y`

Disable host_key_checking in /etc/ansible/ansible.cfg on load-balancer:\
`host_key_checking = False`

Run the setup.sh file (runs ansible-playbooks to configure load-balancer and web apps):\
`bash setup.sh`

Test out the load balancer:\
`bash test-load-balancer.sh`

`random comment added to
